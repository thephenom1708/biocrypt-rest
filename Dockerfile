# pull official base image
FROM python:3.8.1

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# set work directory
RUN mkdir /app
RUN mkdir /app/rest
WORKDIR /app/rest

# install dependencies
RUN pip install --upgrade pip
RUN pip install --upgrade setuptools
COPY ./requirements.txt /app/rest
RUN pip install -r requirements.txt

COPY . /app/rest
