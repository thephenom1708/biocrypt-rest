from django.conf.urls import url, include

app_name = 'api'

urlpatterns = [
    url(r'^auth/', include('authentication.api_urls', namespace='auth_api')),
    url(r'^shares/', include('shares.api_urls', namespace='shares_api')),
]
