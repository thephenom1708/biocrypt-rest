from django.conf.urls import url

from shares.views import ShareCreateView, ShareListAPIView, ShareRetrieveView

app_name = 'shares_api'

urlpatterns = [
    url(r'^post/(?P<user_id>[0-9a-zA-Z]+)/(?P<fingerprint_id>[0-9a-zA-Z]+)/$',
        ShareCreateView.as_view(), name='shares-create-api'),

    url(r'^get/(?P<user_id>[0-9a-zA-Z]+)/$',
        ShareListAPIView.as_view(), name='shares-list-api'),

    url(r'^mapping/get/(?P<user_id>[0-9a-zA-Z]+)/(?P<fingerprint_id>[0-9a-zA-Z]+)/$',
        ShareRetrieveView.as_view(), name='shares-retrieve-api')
]
