from uuid import uuid4

from django.db import models
import secrets


class Share(models.Model):
    id = models.UUIDField(default=uuid4, editable=True, primary_key=True)
    share_number = models.CharField(max_length=10, default="1")
    share_data = models.TextField()

    def create_new_share(self, share_number, share_data):
        self.id = secrets.token_hex(10)
        self.share_number = share_number
        self.share_data = share_data

    def __str__(self):
        return str(self.share_number) + " -- " + str(self.id)


class UserShareMapping(models.Model):
    user_id = models.CharField(max_length=100)
    fingerprint_id = models.CharField(max_length=50)
    shares = models.ManyToManyField(Share, related_name='user_share_mappings')

    def __str__(self):
        return str(self.user_id) + "--" + str(self.fingerprint_id)
