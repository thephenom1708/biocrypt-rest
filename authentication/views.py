from rest_framework import generics, permissions, viewsets
from rest_framework.response import Response

from authentication.models import User
from authentication.serializers import RegisterSerializer, UserSerializer, LoginSerializer


class RegisterAPI(generics.CreateAPIView):
    permission_classes = [
        permissions.AllowAny
    ]
    serializer_class = RegisterSerializer

    def post(self, request, *args, **kwargs):
        user = User()
        user.createNewUser(
            name=request.data.get('name'),
            username=request.data.get('username'),
            pin=request.data.get('pin')
        )
        user.save()
        return Response({
            "user": UserSerializer(user, context=self.get_serializer_context()).data,
        })


# Login API
class LoginAPI(generics.GenericAPIView):
    serializer_class = LoginSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data
        return Response({
            "user": UserSerializer(user, context=self.get_serializer_context()).data,
        })


class UserViewSet(viewsets.ModelViewSet):
    permission_classes = [
        permissions.AllowAny
    ]
    serializer_class = UserSerializer
    lookup_field = 'id'

    def get_queryset(self):
        return User.objects.all()
