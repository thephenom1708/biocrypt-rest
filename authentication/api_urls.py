from django.conf.urls import url
from rest_framework import routers
from authentication.views import RegisterAPI, LoginAPI, UserViewSet

app_name = 'auth_api'

router = routers.SimpleRouter()
router.register('', UserViewSet, 'users-api')


urlpatterns = [
    url(r'^register/$', RegisterAPI.as_view(), name='register-api'),
    url(r'^login/$', LoginAPI.as_view(), name='login-api'),
]

urlpatterns += router.urls
