import hashlib

from rest_framework import serializers

from authentication.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'


# Register Serializer
class RegisterSerializer(serializers.Serializer):
    name = serializers.CharField()
    username = serializers.CharField()
    pin = serializers.CharField()


# Login Serializer
class LoginSerializer(serializers.Serializer):
    username = serializers.CharField()
    pin = serializers.CharField()

    def validate(self, data):
        pin = data.get('pin')
        sha = hashlib.sha256()
        sha.update(str(pin).encode('utf-8'))
        pin_hash = sha.hexdigest()

        user = User.objects.filter(username=data.get('username'), pin_hash=pin_hash).first()
        if user:
            return user
        raise serializers.ValidationError("Invalid Credentials! Please try again!")
