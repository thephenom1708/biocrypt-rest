from django.contrib import admin
from django.contrib.auth.models import Group
from .models import User


class UserAdmin(admin.ModelAdmin):
    list_display = ['name', 'username', 'id']
    ordering = ['id']
    search_fields = ['name', 'username', 'id']


admin.site.register(User, UserAdmin)
admin.site.unregister(Group)
